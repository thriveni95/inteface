package interfacemethods;

import java.util.Scanner;

public class Helper {
   /*
    * this is a helper class used to call all the methods 
    */
	public static void main(String args[]) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter your choice:");
		System.out.println("1.Circle");
		System.out.println("2.Rectangle");
		System.out.println("3.Square");
		int ch=sc.nextInt();
		switch(ch) {
		case 1:
			System.out.println("Enter the radius");
			int radius=sc.nextInt();
			Circle c1=new Circle();
			c1.radius=radius;
			c1.area();
			c1.diameter();
			c1.perimeter();
			break;
		case 2:
			Rectangle r1=new Rectangle1();
			System.out.println("enter the length of a rectangle:");
			int length=sc.nextInt();
			System.out.println("enter the breadth of a rectangle:");
			int breadth=sc.nextInt();
			System.out.println("enter the volume of a rectangle:");
			int height=sc.nextInt();
			r1.length=length;
			r1.breadth=breadth;
			r1.height=height;
			r1.perimeter();
			r1.area();
			r1.volume();
			break;
		case 3:
			Rectangle s1=new Square();
			System.out.println("enter the side of a square:");
			int side=sc.nextInt();
			s1.side=side;
			s1.perimeter();
			s1.area();
			s1.volume();
			break;
		}	
	}
}
