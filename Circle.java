package interfacemethods;
/*
 * This is the circle class which implements the interface shape
 * and override the methods in interface
 */
public class Circle implements Shape {
	//Declare the constant pi and assign value
	private static final float pi = 3.14f;
	int radius;
	 
	public void perimeter() {
		System.out.println("Perimeter of the circle:"+2*pi*radius);
	}
	public void area() {
				System.out.println("Area of the circle:"+pi*radius*radius);
	}
   public void diameter() {
	   System.out.println("Diameter of the circle:"+2*radius);
   }
}
/*
 * This is the  abstract rectangle class which implements the interface shape
 * and override the methods in interface
 */
   abstract class Rectangle implements Shape{
      int length;
      int breadth;
      int height;
      int side;
	public abstract void perimeter();
	public abstract void area();
	public abstract void volume();
	public void helper() {
		System.out.println("This is a non abstract method");
	}
}
   /*
    * Creating a Rectangle1 class which calls abstract  methods and non-abstract methods
    * in Rectangle
    */
   class Rectangle1 extends Rectangle{
	   public void perimeter() {
			int perimeter=2*(length+breadth);
			System.out.println("perimeter of a Rectangle"+perimeter);
		}
		@Override
		public void area() {
			int area=length*breadth;
			System.out.println("Area of the Rectangle"+area);
		}
		@Override
		public void volume() {
			int volume=length*breadth*height;
			System.out.println("Volume of the Rectangle"+volume);
			
		}
   }
   /*
    * Creating a Square  class which calls abstract  methods and non-abstract methods
    * in Rectangle
    */

   class Square extends Rectangle{
	@Override
	public void perimeter() {
		int perimeter=4*side;
		System.out.println("perimeter of a Square"+perimeter);
	}

	@Override
	public void area() {
		int area=side*side;
		System.out.println("Area of the Square:"+area);
	}

	@Override
	public void volume() {
		int volume=side*side*side;
		System.out.println("Volume of the square:"+volume);
	}
	   
   }
